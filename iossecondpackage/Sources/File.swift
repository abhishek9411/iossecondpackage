//
//  File.swift
//  
//
//  Created by Abhishek Singh on 30/06/23.
//

import Foundation
import SwiftUI

@available(ios 13.0, *)
public struct FirstView: View {
    public init() {}
    @available(macOS 10.15.0, *)
    public var body: some View {
        Text("this is first app")
    }
}
